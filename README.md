# DB Ensurer (MySQL)

Job for running within a Docker Compose setup to create a new database with [the `db-maker` service](https://gitlab.com/mycf.sg/img-db-maker-mysql). Run this job to create the required database and database user.

# Usage

See the [docker-compose.yml](./docker-compose.yml) for a simple setup.

# Configuration

| Environment Variable | Type   | Description                                                                          |
| -------------------- | ------ | ------------------------------------------------------------------------------------ |
| `DB_MAKER_URL`       | String | Specifies the base URL of the `db-maker` service. Eg. `http://db-maker:8080`         |
| `DB_DRIVER`          | String | Specifies the DB driver to use. Eg. `mysql` or `pg` (defaults to `mysql`)            |
| `DB_HOST`            | String | Specifies the hostname of the database. Eg. `mysql` (port defaults to 3306)          |
| `DB_NAME`            | String | Specifies the name of the new schema you wish to create. Eg. `my_schema`             |
| `DB_USER`            | String | Specifies the user which will be granted access to the new schema. Eg. `schema_user` |
| `DB_PASSWORD`        | String | Specifies the password of the user specified in `DB_USER`                            |
| `DB_ROOT_PASSWORD`   | String | Specifies the root password so that the user can be created                          |
| `MAX_RETRY`          | String | Specifies the maximum number of retries of the job before it exits with 1            |
| `INTERVAL_RETRY`     | String | Specifies the duration in milliseconds between connection retries                    |

# Development

## Version Bumping

To bump a version, [run a pipeline](https://gitlab.com/mycf.sg/img-db-ensurer-mysql/pipelines/new) with the input variable `VERSION_BUMP_TYPE` set to one of `"major"` or `"minor"`. It defauilts to `"patch"`.

# Re-Use

## Pipeline Configuration

| Pipeline Variable       | Description                                                                                                 | Example Value        |
| ----------------------- | ----------------------------------------------------------------------------------------------------------- | -------------------- |
| IMAGE_NAME              | Name of the Docker image (eg. for DockerHub, docker.io/org/****image****:tag)                               | `"db-ensurer-mysql"` |
| IMAGE_NAMESPACE         | Namespace of the Docker image (eg. for DockerHub, docker.io/****org****/image:tag)                          | `"mycfsg"`           |
| IMAGE_REGISTRY_PASSWORD | Password for the registry user identified in `IMAGE_REGISTRY_USERNAME`                                      | `"password"`         |
| IMAGE_REGISTRY_URL      | URL of the image registry without the http/https protocol prefix                                            | `"docker.io"`        |
| IMAGE_REGISTRY_USERNAME | Username of the registry user                                                                               | `"user"`             |
| SSH_DEPLOY_KEY          | Base64 encoded private key corresponding to a registered Deploy Key (see below section on Deploy Key Setup) | `null`               |

## Deploy Key Setup

Generate a key using:

```bash
ssh-keygen -t rsa -b 8192 -f ./img-db-ensurer-mysql -q -N ""
```

Run the private key through a base64 encoding and copy the output:

```bash
cat ./img-db-ensurer-mysql | base64 -w 0
```

Paste the value in the pipeline variable `SSH_DEPLOY_KEY`.

Copy the contents of the public key and paste the value in the Deploy Keys section of the CI/CD->Repository settings page in your repository.

## License

This project is licensed under [the MIT license](./LICENSE).
